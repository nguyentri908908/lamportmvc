﻿using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Security.Cryptography;
using System.Text;


namespace SigLamPort.Controllers
{
    public class VerifyController : Controller
    {
        public IActionResult Index()
        {
            return View();
        }
        public IActionResult Upload()
        {
            string message = Request.Form["Message"];
            string readJson1 = System.IO.File.ReadAllText("./wwwroot/CurrentFile/public_key.json");
            var readData1 = JsonConvert.DeserializeObject<dynamic>(readJson1);
            List <List<byte[]>> pk = readData1.ToObject<List<List<byte[]>>>();
            string readJson2 = System.IO.File.ReadAllText("./wwwroot/CurrentFile/signature.json");
            byte[] sig = JsonConvert.DeserializeObject<byte[]>(readJson2);
            bool result;
            if (Verify(message, sig, pk))
            {
                result = true;
            }
            else result = false;
            ViewBag.Result = result;
            return View();
        }
        static bool Verify(string m, byte[] sig, List<List<byte[]>> pk)
        {
            var h = BitConverter.ToUInt64(HashBytes(Encoding.UTF8.GetBytes(m)), 0);
            var sigChunks = SplitArray(sig, 32);
            for (int i = 0; i < 256; i++)
            {
                var b = (int)((h >> i) & 1);
                var check = HashBytes(sigChunks[i]);
                if (!ArraysEqual(pk[b][i], check))
                {
                    return false;
                }
            }
            return true;
        }
        static byte[] HashBytes(byte[] input)
        {
            using (var sha256 = SHA256.Create())
            {
                return sha256.ComputeHash(input);
            }
        }

        static byte[][] SplitArray(byte[] array, int chunkSize)
        {
            var chunks = new byte[array.Length / chunkSize][];
            for (int i = 0; i < chunks.Length; i++)
            {
                chunks[i] = new byte[chunkSize];
                Array.Copy(array, i * chunkSize, chunks[i], 0, chunkSize);
            }

            return chunks;
        }

        static bool ArraysEqual(byte[] a1, byte[] a2)
        {
            if (a1.Length != a2.Length)
                return false;

            for (int i = 0; i < a1.Length; i++)
            {
                if (a1[i] != a2[i])
                    return false;
            }

            return true;
        }
        [HttpPost]
        public async Task<IActionResult> WriteToFile1([FromBody] string content)
        {
            try
            {
                string filePath = "./wwwroot/CurrentFile/signature.json";

                // Ghi nội dung tệp tin
                await System.IO.File.WriteAllTextAsync(filePath, content);

                return Ok(); // Trả về mã HTTP 200 OK
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message); // Trả về mã HTTP 500 Internal Server Error nếu có lỗi
            }
        }
        [HttpPost]
        public async Task<IActionResult> WriteToFile2([FromBody] string content)
        {
            try
            {
                string filePath = "./wwwroot/CurrentFile/public_key.json";

                // Ghi nội dung tệp tin
                await System.IO.File.WriteAllTextAsync(filePath, content);

                return Ok(); // Trả về mã HTTP 200 OK
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message); // Trả về mã HTTP 500 Internal Server Error nếu có lỗi
            }
        }
    }
}
