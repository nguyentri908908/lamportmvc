﻿using Microsoft.AspNetCore.Mvc;
using System.Security.Cryptography;
using System.Text;

namespace SigLamPort.Controllers
{
    public class LamportController : Controller
    {
        public IActionResult Index()
        {
            return View();
        }
        string message = "";
        string re = "";
        (List<List<byte[]>> sk, List<List<byte[]>> pk) key;
        byte[] sign;
        public IActionResult SignResult()
        {
            message = Request.Form["message"];
            var keypair = Keygen();
            key = keypair;
            var jsonData = Newtonsoft.Json.JsonConvert.SerializeObject(key.pk);
            var fileName = "./wwwroot/CurrentFile/public_key.json";
            System.IO.File.WriteAllText(fileName, jsonData);
            StringBuilder reKey = new StringBuilder();
            foreach (var list in key.pk)
            {
                foreach (var byteArray in list)
                {
                    reKey.AppendLine(Convert.ToBase64String(byteArray));
                }
            }
            //reKey.Append(key.ToString());
            ViewBag.Key = reKey;
            var signature = Sign(message, keypair.sk);
            sign = signature;
            var signatureBase64 = Newtonsoft.Json.JsonConvert.SerializeObject(sign);
            fileName = "./wwwroot/CurrentFile/signature.json";
            System.IO.File.WriteAllText(fileName, signatureBase64);
            StringBuilder reSig = new StringBuilder();
            reSig.Append(Convert.ToBase64String(sign));
            ViewBag.Sig = reSig;
            //re = Verify(message, signature, keypair.pk).ToString();
            return View();
        }
        
        const string CHAR_ENC = "utf-8";
        const ByteOrder BYTE_ORDER = ByteOrder.LittleEndian;
        enum ByteOrder
        {
            LittleEndian,
            BigEndian
        }
        static (List<List<byte[]>> sk, List<List<byte[]>> pk) Keygen()
        {
            var sk = new List<List<byte[]>>();
            var pk = new List<List<byte[]>>();
            for (int i = 0; i < 2; i++)
            {
                sk.Add(new List<byte[]>());
                pk.Add(new List<byte[]>());
                for (int j = 0; j < 256; j++)
                {
                    // secret key
                    sk[i].Add(GenerateRandomBytes(32));
                    // public key
                    pk[i].Add(HashBytes(sk[i][j]));
                }
            }

            return (sk, pk);
        }

        static byte[] Sign(string m, List<List<byte[]>> sk)
        {
            var sig = new List<byte>();
            var h = BitConverter.ToUInt64(HashBytes(Encoding.UTF8.GetBytes(m)), 0);
            for (int i = 0; i < 256; i++)
            {
                var b = (int)((h >> i) & 1);
                sig.AddRange(sk[b][i]);
            }

            return sig.ToArray();
        }

        
        static byte[] GenerateRandomBytes(int length)
        {
            using (var rng = new RNGCryptoServiceProvider())
            {
                var bytes = new byte[length];
                rng.GetBytes(bytes);
                return bytes;
            }
        }

        static byte[] HashBytes(byte[] input)
        {
            using (var sha256 = SHA256.Create())
            {
                return sha256.ComputeHash(input);
            }
        }

        static byte[][] SplitArray(byte[] array, int chunkSize)
        {
            var chunks = new byte[array.Length / chunkSize][];
            for (int i = 0; i < chunks.Length; i++)
            {
                chunks[i] = new byte[chunkSize];
                Array.Copy(array, i * chunkSize, chunks[i], 0, chunkSize);
            }

            return chunks;
        }

        static bool ArraysEqual(byte[] a1, byte[] a2)
        {
            if (a1.Length != a2.Length)
                return false;

            for (int i = 0; i < a1.Length; i++)
            {
                if (a1[i] != a2[i])
                    return false;
            }

            return true;
        }
    }
}
